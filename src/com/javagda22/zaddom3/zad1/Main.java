package com.javagda22.zaddom3.zad1;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("A", "B", "123");

        // ustawienie ocen przez setter powoduje zastąpienie
        // wcześniejszej tablicy 100 elementowej na podaną w parametrze.
        double[] tablica = new double[]{3.0, 3.0, 5.0, 5.0, 5.0};
        student.setOceny(tablica);

        // obliczenie średniej zwraca wynik ORAZ wypisuje komunikat na
        // konsoli.
        double srednia = student.obliczSrednia();
        System.out.println(srednia);

        //
        System.out.println(student.czyDobryStudent());
    }
}
