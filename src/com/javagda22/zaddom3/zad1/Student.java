package com.javagda22.zaddom3.zad1;

public class Student {
    private String imie;
    private String nazwisko;
    private String nrIndeksu;
    private double[] oceny = new double[100];

    public Student(String imie, String nazwisko, String nrIndeksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndeksu = nrIndeksu;
        this.oceny = new double[100];
    }

    public double obliczSrednia() {
        double suma = 0.0; // inicjalizacja zmiennej suma
        for (int i = 0; i < oceny.length; i++) {
            suma += oceny[i]; // sumujemy oceny
        }

        // wypisujemy komunikat
        System.out.println("średnia: " + (suma / oceny.length));

        return suma / oceny.length; // średnia
    }

    public boolean czyDobryStudent() {
        for (int i = 0; i < oceny.length; i++) {
//            if(oceny[i] <= 2){
            if (oceny[i] == 1 || oceny[i] == 2) {
                // wydarzy się jeden raz, jak tylko trafi na pierwszą ocenę tego typu
                return false; // nie jest dobrym uczniem
            }
        }
        return true; // jest dobrym uczniem
    }

    public double[] getOceny() {
        return oceny;
    }

    public void setOceny(double[] oceny) {
        this.oceny = oceny;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(String nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }
}
