package com.javagda22.zaddom3.zad2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę od 1 do 20:");
        int liczba = scanner.nextInt();
        if (liczba <= 20 && liczba >= 1) {

            for (int zakres = 1; zakres <= liczba; zakres++) {
                // rozszerzanie wenętrznej pętli
                for (int j = 1; j <= zakres; j++) { // 1 / 12 / 123 / 1234 ...
                    System.out.print(j);
                }
                System.out.println();
            }

        } else {
            System.out.println("Liczba jest poza zakresem");
        }
    }
}
